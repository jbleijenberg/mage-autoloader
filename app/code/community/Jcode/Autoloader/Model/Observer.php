<?php

/**
 * NOTICE OF LICENSE
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 *
 * @category    mage-autoloader
 * @package     mage-autoloader
 * @author      Jeroen Bleijenberg <jeroen@jcode.nl>
 * @copyright   Copyright (c) 2015 Jcode (http://www.jcode.nl)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class Jcode_Autoloader_Model_Observer
{

	const SCOPE_FILE_PREFIX = '__';

	static protected $_instance;

	static protected $_scope = 'default';

	protected $_isIncludePathDefined = null;

	protected $_collectClasses = false;

	protected $_collectPath = null;

	protected $_arrLoadedClasses = array();

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		register_shutdown_function(array($this, 'destroy'));
		$this->_isIncludePathDefined = defined('COMPILER_INCLUDE_PATH');
		if (defined('COMPILER_COLLECT_PATH')) {
			$this->_collectClasses = true;
			$this->_collectPath = COMPILER_COLLECT_PATH;
		}
		self::registerScope(self::$_scope);
	}

	/**
	 * Singleton pattern implementation
	 *
	 * @return Varien_Autoload
	 */
	static public function instance()
	{
		if (!self::$_instance) {
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	/**
	 * Register SPL autoload function
	 */
	static public function register()
	{
		spl_autoload_register(array(self::instance(), 'autoload'));
	}

	/**
	 * Add a custom autoloader to make use of namespaces is custom modules.
	 *
	 */
	public function setAutoloader()
	{
		$autoloader = function ($className) {
			$className = ltrim($className, '\\');

			if ($lastNsPos = strripos($className, '\\')) {
				$namespace = substr($className, 0, $lastNsPos);
				$className = substr($className, $lastNsPos + 1);

				$fileName = str_replace('\\', DS, $namespace) . DS;

				$fileName .= str_replace('_', DS, $className) . '.php';

				if (stream_resolve_include_path($fileName) !== false) {
					return include $fileName;
				}
			} else {
				if ($this->_collectClasses) {
					$this->_arrLoadedClasses[self::$_scope][] = $className;
				}
				if ($this->_isIncludePathDefined) {
					$classFile = COMPILER_INCLUDE_PATH . DIRECTORY_SEPARATOR . $className;
				} else {
					$classFile = str_replace(' ', DIRECTORY_SEPARATOR, ucwords(str_replace('_', ' ', $className)));
				}
				$classFile .= '.php';

				return include $classFile;
			}
		};

		$autoloader_callbacks = spl_autoload_functions();
		$original_autoload = null;

		foreach ($autoloader_callbacks as $callback) {
			if (is_array($callback) && $callback[0] instanceof Varien_Autoload) {
				$original_autoload = $callback;
			}
		}

		if ($original_autoload) {
			spl_autoload_unregister($original_autoload);
		}
		
		spl_autoload_register($autoloader);
	}

	/**
	 * Register autoload scope
	 * This process allow include scope file which can contain classes
	 * definition which are used for this scope
	 *
	 * @param string $code scope code
	 */
	static public function registerScope($code)
	{
		self::$_scope = $code;
		if (defined('COMPILER_INCLUDE_PATH')) {
			@include COMPILER_INCLUDE_PATH . DIRECTORY_SEPARATOR . self::SCOPE_FILE_PREFIX . $code . '.php';
		}
	}

	/**
	 * Get current autoload scope
	 *
	 * @return string
	 */
	static public function getScope()
	{
		return self::$_scope;
	}

	/**
	 * Class destructor
	 */
	public function destroy()
	{
		if ($this->_collectClasses) {
			$this->_saveCollectedStat();
		}
	}

	/**
	 * Save information about used classes per scope with class popularity
	 * Class_Name:popularity
	 *
	 * @return Varien_Autoload
	 */
	protected function _saveCollectedStat()
	{
		if (!is_dir($this->_collectPath)) {
			@mkdir($this->_collectPath);
			@chmod($this->_collectPath, 0777);
		}

		if (!is_writeable($this->_collectPath)) {
			return $this;
		}

		foreach ($this->_arrLoadedClasses as $scope => $classes) {
			$file = $this->_collectPath . DIRECTORY_SEPARATOR . $scope . '.csv';
			$data = array();
			if (file_exists($file)) {
				$data = explode("\n", file_get_contents($file));
				foreach ($data as $index => $class) {
					$class = explode(':', $class);
					$searchIndex = array_search($class[0], $classes);
					if ($searchIndex !== false) {
						$class[1] += 1;
						unset($classes[$searchIndex]);
					}
					$data[$index] = $class[0] . ':' . $class[1];
				}
			}
			foreach ($classes as $class) {
				$data[] = $class . ':1';
			}
			file_put_contents($file, implode("\n", $data));
		}

		return $this;
	}
}