<?php
/**
 * NOTICE OF LICENSE
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade this module to newer
 * versions in the future.
 *
 * @category    mage-autoloader
 * @package     mage-autoloader
 * @author      Jeroen Bleijenberg <jeroen@jcode.nl>
 * @copyright   Copyright (c) 2015 Jcode (http://www.jcode.nl)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
namespace Jcode\Autoloader\Model;

use \Mage;

class Layout extends \Mage_Core_Model_Layout
{

	/**
	 * Create block object instance based on block type
	 *
	 * @param string $block
	 * @param array $attributes
	 *
	 * @return \Mage_Core_Block_Abstract
	 */
	protected function _getBlockInstance($block, array $attributes = array())
	{
		if (is_string($block)) {
			if (strpos($block, '/') !== false) {
				if (!$block = Mage::getConfig()->getBlockClassName($block)) {
					Mage::throwException(Mage::helper('core')->__('Invalid block type: %s', $block));
				}
			}
			if (class_exists($block) || mageFindClassFile($block)) {
				$block = new $block($attributes);
			}
		}
		
		if (!$block instanceof \Mage_Core_Block_Abstract) {
			Mage::throwException(Mage::helper('core')->__('Invalid block type: %s', $block));
		}

		return $block;
	}
}